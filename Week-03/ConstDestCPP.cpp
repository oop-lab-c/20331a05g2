#include<iostream>
using namespace std;
class Student
{
    string fullname;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;
    public:
    Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    void set(string n,int r,double p)
    {
        fullname=n;
        rollNum=r;
        semPercentage=p;
    }
    void display()
    {
        cout<<"    Details    \n";
        cout<<"FullName : "<<fullname<<endl;
        cout<<"Rollnum : "<<rollNum<<endl;
        cout<<"SemPercentage : "<<semPercentage<<endl;
        cout<<"CollegeName : "<<collegeName<<endl;
        cout<<"CollegeCode : "<<collegeCode<<endl;
    }
    ~Student()
    {
        cout<<"Destructor called\n";
    }
};
int main()
{
    Student s;
    string x;
    int y;
    double z;
    cout<<"Enter fullname : ";
    cin>>x;
    cout<<"Enter rollnum : ";
    cin>>y;
    cout<<"Enter sempercentage : ";
    cin>>z;
    s.set(x,y,z);
    s.display();
}
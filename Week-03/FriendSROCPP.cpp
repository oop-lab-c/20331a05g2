#include<iostream>
using namespace std;
class Box
{
  float l,w,h;
  public:
  Box(float length,float width,float height)
  {
    l=length;
    w=width;
    h=height;
  }
  void boxArea(float length,float width)
  {
        cout<<"Area = "<<length*width<<endl;
  }
  void boxVolume(float length,float width,float height);
  friend void displayBoxDimensions(Box);
  inline void displayWelcomeMessage()
  {
    cout<<"Welcome\n";
  }
};
void Box::boxVolume(float length,float width,float height)
{
  cout<<"Volume = "<<length*width*height<<endl;
}
void displayBoxDimensions(Box b)
{
  cout<<"----Box Dimensions----\n";
  cout<<"Length : "<<b.l<<endl;
  cout<<"Width : "<<b.w<<endl;
  cout<<"Height : "<<b.h<<endl;
}
int main()
{
  float len,wid,hei;
  cout<<"Enter length,width and height of the box : ";
  cin>>len>>wid>>hei;
  Box B(len,wid,hei);
  B.displayWelcomeMessage();
  B.boxArea(len,wid);
  B.boxVolume(len,wid,hei);
  displayBoxDimensions(B);
  
}
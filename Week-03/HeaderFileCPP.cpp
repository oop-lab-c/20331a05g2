#include<iostream>
#include "boxArea.h"
#include "boxVolume.h"
#define Morning
int main()
{
    float l,w,h;
    cout<<"Enter length,width and height of the box : ";
    cin>>l>>w>>h;
    #ifdef Morning
        boxArea(l,w);
    #ifndef Night
        boxVolume(l,w,h);
    #endif
    #endif
}
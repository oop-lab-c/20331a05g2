#include<iostream>
using namespace std;
class Student
{
    string fullname;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;
    public:
    Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    Student(string N,double P)
    {
        fullname=N;
        semPercentage=P;
    }
    void setroll(int r)
    {
        rollNum=r;
    }
    void display()
    {
        cout<<"    Details    \n";
        cout<<"FullName : "<<fullname<<endl;
        cout<<"Rollnum : "<<rollNum<<endl;
        cout<<"SemPercentage : "<<semPercentage<<endl;
    }
    void display1()
    {
        cout<<"CollegeName : "<<collegeName<<endl;
        cout<<"CollegeCode : "<<collegeCode<<endl;
    }
    ~Student()
    {}
};
int main()
{
    Student S;
    string name;
    int roll;
    double per;
    cout<<"Enter name,rollnumber and sem percentage : ";
    cin>>name>>roll>>per;
    Student S1(name,per);
    S1.setroll(roll);
    S1.display();
    S.display1();
}
class Table{  
    synchronized void printTable(int n)
    {
      for(int i=1;i<=5;i++)
      {  
        System.out.println(n*i);  
        try
        {  
         Thread.sleep(400);  
        }
        catch(Exception e)
        {
            System.out.println(e);
        }  
      }  
     
    }  
   }    
   class MyThread1 extends Thread{  
   Table obj;  
   MyThread1(Table t){  
       obj=t;  
   }  
   public void run(){  
   obj.printTable(5);  
   }  
     
   }  
   class MyThread2 extends Thread{  
   Table obj;  
   MyThread2(Table t){  
       obj=t;  
   }  
   public void run(){  
   obj.printTable(100);  
   }  
   }  
     
   public class ThreadSyncJava{  
   public static void main(String args[]){  
   Table obj = new Table();
   MyThread1 t1=new MyThread1(obj);  
   MyThread2 t2=new MyThread2(obj);  
   t1.start();  
   t2.start();  
   }  
   }  
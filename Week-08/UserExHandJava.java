class MyException extends Exception {
    MyException(String str)
    {
        super(str);
    }
}
public class UserExHandJava
{
    public static void main(String args[])
    {
        try 
        {
            throw new MyException("Hello!! This is MyException...");
        }
        catch (MyException ex) {
            System.out.println("Caught");
            System.out.println(ex.getMessage());
        }
    }
}


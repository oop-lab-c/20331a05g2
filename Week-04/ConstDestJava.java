import java.util.*;
class Student
{
    private String fullname;
    private int rollNum;
    private double semPercentage;
    private String collegeName;
    private int collegeCode;
   
    public Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    public void set(String n,int r,double p)
    {
        fullname=n;
        rollNum=r;
        semPercentage=p;
    }
    public void display()
    {
        System.out.println("    Details    ");
        System.out.println("Fullname : "+fullname);
        System.out.println("RollNumber : "+rollNum);
        System.out.println("Sempercentage : "+semPercentage);
        System.out.println("College Name : "+collegeName);
        System.out.println("College Code : "+collegeCode);
    }
    protected void finalize()
    {
        System.out.println("Destructor called...\n");
    }

    public static void main(String args[])
    {
        Student obj=new Student();
        String x;
        int y;
        double z;
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the Name : ");
        x=sc.nextLine();
        System.out.println("Enter the rollno : ");
        y=sc.nextInt();
        System.out.println("Enter the sem precentage : ");
        z=sc.nextDouble();
        obj.set(x,y,z);
        obj.display();
    }
}    

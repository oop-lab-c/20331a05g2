import java.util.*;
class Student
{
    String collegeName, name;
    int collegeCode, rollNo;
    double semPercentage;
    public Student(){
        collegeName = "MVGR";
        collegeCode = 33;
    }
    public Student(String nam, int r, double sem)
    { 
        name = nam;
        rollNo = r;
        semPercentage = sem;
    }
    void display()
    {
        System.out.println("college code = "+collegeCode);
        System.out.println("college name = "+collegeName);        
    }
    void display1()
    {
        System.out.println("    Details    ");
        System.out.println("Full name = "+name);
        System.out.println("Roll no = "+ rollNo);
        System.out.println("sem percentage = "+semPercentage);        
    }
    protected void finalize(){
        System.out.println("Destructor called...");
    }
    public static void main(String args[]) {
        Student s = new Student();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Name : ");
        String name = sc.nextLine();
        System.out.println("Enter the rollno : ");
        int r = sc.nextInt();
        System.out.println("Enter the sem precentage : ");
        double sem = sc.nextDouble();
        Student s1 = new Student(name,r,sem);
        s1.display1();
        s.display();
    }
}

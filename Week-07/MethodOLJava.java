class Hello
{
    void overload()
    {
        System.out.println("Hello");
    }
    void overload(String name)
    {
        System.out.println("Hello "+name);
    }
}
class MethodOLJava{
    public static void main(String args[])
    {
        Hello obj=new Hello();
        obj.overload();
        obj.overload("Balaji");
    }
}
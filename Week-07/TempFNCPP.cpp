#include <iostream>
using namespace std;
template <class T>                        
class Number {
   private:
    T num;
   public:
    T getNum(T x) {
        num=x;
        return num;
    }
};
int main()
{
    Number<int> numberInt;
    Number<double> numberDouble;
    cout << "int Number = " << numberInt.getNum(7) << endl;
    cout << "double Number = " << numberDouble.getNum(7.7) << endl;
    return 0;
}
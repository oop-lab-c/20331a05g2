class JAVA
{
    public int add(int a,int b)
    {
        return a+b;
    }
}
class CPP extends JAVA
{
    public int add(int a,int b,int c)
    {
        return a+b+c;
    }
}
class MethodOLInheriJava
{
    public static void main(String args[])
    {
        CPP obj=new CPP();
        System.out.println("Addition of two numbers : "+obj.add(10,15));
        System.out.println("Addition of threee numbers : "+obj.add(50,30,20));
    }
}
abstract class Lunch
{
    void drink()
    {
        System.out.println("-> I Drink Water");
    }
    abstract void eat();
}
class Remaining_days extends Lunch
{
    Remaining_days()
    {
        System.out.println("---Remaining Days--- ");
    }
    void eat()
    {
        System.out.println("-> I eat Normal lunch...");
    }
}
class Sunday extends Lunch
{
    Sunday()
    {
        System.out.println("---Sunday---");
    }
    void eat()
    {
        System.out.println("-> I eat Chicken Biryani...");
    }
}
public class ParAbsJava {
    public static void main(String args[])
    {
        Sunday obj=new Sunday();
        obj.drink();
        obj.eat();
        Remaining_days obj1=new Remaining_days();
        obj1.drink();
        obj1.eat();
    }
}

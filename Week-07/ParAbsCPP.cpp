#include<iostream>
using namespace std;
class Dad
{
    public:
    void shirt()
    {
        cout<<"Shirt is selected by Dad.\n";
    }
    virtual void pant();
};
class Me:public Dad
{
    public:
    void pant()
    {
        cout<<"But Pant is of my choice.\n";
    }
};
int main()
{
    Me obj;
    obj.shirt();
    obj.pant();
}
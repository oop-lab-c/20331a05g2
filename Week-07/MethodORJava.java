class Me
{
    void TV()
    {
        System.out.println("I will watch Cricket Match");
    }
}
class MyMom extends Me
{
    void TV()
    {
        System.out.println("I will watch Serial.\nMom Won.");
    }
}
public class MethodORJava 
{
    public static void main(String args[])
    {
        MyMom obj=new MyMom();
        obj.TV();
    }
}

#include<iostream>
using namespace std;
class overload
{
    public:
    int add(int a,int b)
    {
        return a+b;
    }
    int add(int a,int b,int c)
    {
        return a+b+c;
    }
};
int main()
{
    overload obj;
    cout<<"Method overloading\n";
    cout<<"The sum of two numbers is "<<obj.add(100,200)<<endl;
    cout<<"The sum of three numbers is "<<obj.add(100,200,300)<<endl;
}
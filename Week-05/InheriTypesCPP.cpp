#include<iostream>
using namespace std;
class Earth
{
    public:
    Earth()
    {
        cout<<"I am Earth"<<endl;
    }
};
class Asia:public Earth
{
    public:
    Asia()
    {
        cout<<"I am Asia"<<endl;
    }
};
class India:public Asia
{
    public:
    India()
    {
        cout<<"I am India"<<endl;
    }
};
class South
{
    public:
    South()
    {
        cout<<"I am South"<<endl;
    }
};
class Andhra:public South,public India
{
    public:
    Andhra()
    {
        cout<<"I am Andhra"<<endl;
    }
};
class Sea
{
    public:
    Sea()
    {
        cout<<"I am Sea"<<endl;
    }
};
class Vizianagaram:public Andhra
{
    public:
    Vizianagaram()
    {
        cout<<"I am Vizianagaram"<<endl;
    }
};
class Visakhapatnam:public Andhra,public Sea
{
    public:
    Visakhapatnam()
    {
        cout<<"I am Visakhapatnam"<<endl;
    }
};
int main()
{
    cout<<"  Single inheritance  "<<endl;
    Asia b;
    cout<<"  Multilevel inheritance  "<<endl;
    India c;
    cout<<"  Multiple inheritance  "<<endl;
    Andhra d;
    cout<<"  Heirarchial inheritance  "<<endl;
    Vizianagaram f;
    cout<<"  Hybrid inheritance  "<<endl;
    Visakhapatnam e;
}
class Earth
{
    Earth()
    {
        System.out.println("I am Earth");
    }
}
class Asia extends Earth
{
    Asia()
    {
        System.out.println("I am Asia");
    }
}
class India extends Asia
{
    India()
    {
        System.out.println("I am India");
    }
}
class Andhra extends India
{
    Andhra()
    {
        System.out.println("I am Andhra");
    }
}
class Telangana extends India
{
    Telangana()
    {
        System.out.println("I am Telangana");
    }
}
public class InheriTypes {
    public static void main(String args[])
    {
        System.out.println("  Single inheritance  ");
        Asia a=new Asia();
        System.out.println("  Multilevel inheritance  ");
        India b=new India();
        System.out.println("  Heirarchial inheritance  ");
        Andhra c=new Andhra();
        Telangana d= new Telangana();
    }
}

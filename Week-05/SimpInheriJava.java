class ICC
{
    void icc_tournaments()
    {
        System.out.println("World Cup");
        System.out.println("T20 World Cup");
        System.out.println("World Test Championship");
        System.out.println("International matches");
    }
}
class BCCI extends ICC
{
    void bcci_tournaments()
    {
        System.out.println("IPL");
        System.out.println("Ranji Trophy");
        System.out.println("Vijay Hazare Trophy");
    }
}
public class SimpInheri
{
    public static void main(String args[])
    {
        //B obj=new B();
        BCCI obj=new BCCI();
        System.out.println("----Tournamnets under ICC----");
        obj.icc_tournaments();
        System.out.println("----Tournaments under BCCI----");
        obj.bcci_tournaments();

    }
}

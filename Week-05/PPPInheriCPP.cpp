#include<iostream>
using namespace std;
class Base{
    public:
    int a = 10;
    int getC()
    {
        return c;
    }
    protected :
    int b = 20;
    private:
    int c = 30;
};
class D1 : public Base
{
    public:
    int getB(){
        return b;
    }
};
class D2 : protected Base
{
    public:
    int getA(){
        return  a;
    }
    int getB(){
        return b;
    }
};
class D3 : private Base
{
    public:
    int getA(){
        return a;
    }
    int getB(){
        return b;
    }
};
int main(int argc, char const *argv[])
{
    D1 obj;
    D2 obj1;
    D3 obj2;
    cout<<"    Public inheritance    "<<endl;
    cout<<"Public var : "<<obj.a<<endl;
    cout<<"Protected var : "<<obj.getB()<<endl;
    cout<<"Private var : "<<obj.getC()<<endl;
    cout<<"    Protected inheritance   "<<endl;
    cout<<"Public var : "<<obj1.getA()<<endl;
    cout<<"Protected var : "<<obj1.getB()<<endl;
    cout<<"getC function gets into protected part.So,Private var cannot be accessed."<<endl;
    cout<<"   Private inheritance   "<<endl;
    cout<<"Public var : "<<obj2.getA()<<endl;
    cout<<"Protected var : "<<obj2.getB()<<endl;
    cout<<"getC function gets into private part.So,Private var cannot be accessed."<<endl;
    return 0;
}
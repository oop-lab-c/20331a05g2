interface Python
{
    public default void display()
    {
        System.out.println("\nSolution for diamond problem in JAVA\n");
    }
}
interface Java extends Python{     
     
}  
interface Cpp extends Python{  

}  
public class MultipleInheri implements Java, Cpp  
{  
    public static void main(String args[])   
    {  
        MultipleInheri obj = new MultipleInheri();  
        obj.display();  
    }  
} 

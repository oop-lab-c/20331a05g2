import java.util.*;

public class Calc {
    public static void main(String args[])
    {
        int x,num1,num2;
        float y;
        char ch;
        Scanner s=new Scanner(System.in);
        System.out.println("Enter first number : ");
        num1=s.nextInt();
        System.out.println("Enter second number : ");
        num2=s.nextInt();
        System.out.println("Enter the arithmetic operator you want to perform on the operands : ");
        ch=s.next().charAt(0);
        if(ch=='+')
        {
            x=num1+num2;
            System.out.println("The sum is "+x);
        }
        else if(ch=='-')
        {
            x=num1-num2;
            System.out.println("The difference is "+x);
        }
        else if(ch=='*')
        {
            x=num1*num2;
            System.out.println("The product is "+x);
        }
        else if(ch=='/')
        {
            y=num1/num2;
            System.out.println("The quotuient is "+y);
        }
        else if(ch=='%')
        {
            x=num1%num2;
            System.out.println("The modulos is "+x);
        }
        else 
        {
            System.out.println("Wrong choice...!");
        }
    }
}

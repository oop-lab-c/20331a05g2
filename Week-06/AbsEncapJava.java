class AccesSpecifierDemo
{
    
    private int priVar;
    protected int proVar;
    public int pubVar;
    public void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public void getVar()
    {
        System.out.println("Private variable : "+priVar);
        System.out.println("Protected variable : "+proVar);
        System.out.println("Public variable : "+pubVar);
    }
}
public class AbsEncap {
    public static void main(String args[])
    {
        AccesSpecifierDemo obj=new AccesSpecifierDemo();
        obj.setVar(25, 50, 75);
        System.out.println("  Access Specifiers  ");
        obj.getVar();
    }
}

#include<iostream>
using namespace std;
class AccessSpecifierDemo
{
    private:
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;
    void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar()
    {
        cout<<"Private variable : "<<priVar<<endl;
        cout<<"Protected variable : "<<proVar<<endl;
        cout<<"Public variable : "<<pubVar<<endl;
    }
};
int main()
{
   AccessSpecifierDemo obj;
   obj.setVar(25,50,75);
   cout<<"  Access Specifiers  \n";
   obj.getVar(); 
}